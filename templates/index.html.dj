<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
 "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="{{ Config.language }}">
  <head>
    <title>{{ name }}</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="generator" content="{{ generator }}" />
    <link rel="shortcut icon" href="static/planetgnu.ico"/>
    <link rel="stylesheet" href="static/planet.css" type="text/css" />
    <link rel="alternate" href="https://planet.gnu.org/atom.xml" title="" type="application/atom+xml">
    <!--
    <link href="rss10.xml" rel="alternate" type="application/rss+xml"  title="RSS 1.0 Feed" />
    <link href="rss20.xml" rel="alternate" type="application/rss+xml"  title="RSS 2.0 Feed" />
    <link href="atom.xml"  rel="alternate" type="application/atom+xml" title="Atom Feed" />
    -->
  </head>

  <body>
    <h1>{{ name }}</h1>
    <p class="header">Aggregation of development blogs from the GNU Project</p>

{% for item in Items %}
  {% ifchanged item.new_date %}
    <div class="daygroup">
      <h2>{{ item.new_date }}</h2>
  {% endifchanged %}

  {% ifchanged item.channel_name %}
      <div class="channelgroup">
    {% if item.channel_face %}
    {% endif %}
        <h3><a href="{{ item.channel_link }}" title="{{ item.channel_title|safe }}">{{ item.channel_name|safe }}</a></h3>
  {% endifchanged %}

        <div id="{{ item.link }}" class="entrygroup">
          <h4><a href="{{ item.link }}">{{ item.title|safe }}</a></h4>
          <div class="entry">
            <div class="content">
              {{ item.content|safe }}
            </div>
            <p class="date">
              <a href="{{ item.link }}">{{ item.date }}</a>
  {% if item.author %}
              by {{ item.author|safe }}
  {% endif %}
            </p>
          </div>
        </div>

  {% ifchanged item.channel_name %}
      </div>
  {% endifchanged %}

  {% ifchanged item.new_date %}
    </div>
  {% endifchanged %}

{% endfor %}


    <div class="sidebar">
      <div align="center">
        <img src="static/logo.png" alt="Planet GNU logo">
        <p>
          <a href="http://www.fsf.org/associate/support_freedom/join_fsf?referrer=2442"><img src="static/thin-image.png" alt="Support freedom" title="Help protect your freedom, join the Free Software Foundation"/></a>
        </p>
      </div>
      <p>
      <strong>Last updated</strong> (hourly):<br>{{ date|date:"d M Y H:i" }}<br><em>All times are UTC.</em><br />
        <br />
        The mailing list for announcements of official releases for all GNU packages is <a href="https://lists.gnu.org/mailman/listinfo/info-gnu">info-gnu@gnu.org</a>.<br />
        Maintainers, please also send announcements there.<br />
        <br />
        Please write to <a href="https://lists.gnu.org/mailman/listinfo/planet">planet@gnu.org</a> (public list) for feed aggregation requests or suggestions.<br />
	<br />
	<a href="https://www.gnu.org/philosophy/kind-communication.html">GNU Kind Communications Guidelines</a>
      </p>

      <ul>
        <li><a href="rss20.xml"><img src="static/rss2.png" width="94" height="15" alt="RSS 2.0 Feed"></a>&nbsp;<a href="rss10.xml"><img src="static/rss1.png" width="94" height="15" alt="RSS 1.0 Feed"></a></li>
        <li><a href="foafroll.xml"><img src="static/foaf.png" width="94" height="15" alt="FOAF Subscriptions"></a>&nbsp;<a href="opml.xml"><img src="static/opml.png" width="94" height="15" alt="OPML Subscriptions"></a></li>
        <li><a href="atom.xml"><img src="static/atom.png" width="80" height="15" alt="Atom Feed"></a>&nbsp;<a href="https://www.intertwingly.net/code/venus/"><img src="static/planet.png" width="80" height="15" alt="Planet"></a></li>
      </ul>

      <p>
        Admin and hosting by <a href="https://www.beuc.net/">Beuc</a>.<br />
        Configuration source available:<br />
        <a href="config/">planet-config</a> (<a href="https://git.savannah.nongnu.org/cgit/gnues/planet-config.git/">git@sv</a>)<nr />
        <a href="infra/">planet-infra</a> (<a href="https://git.savannah.nongnu.org/cgit/gnues/planet-infra.git/">git@sv</a>)<br />
      </p>

      <p>
	This is
	a <a href="https://en.wikipedia.org/wiki/Planet_(software)">Planet</a>
	a.k.a. a news aggregator.  We review and add feeds (listed
	below), not individual articles.  Savannah GNU feeds are
	automatically added hourly.
      </p>

{% comment %}can't get it to work with %regroup, proceeding manually; removing "GNUplanet RSS Feeds" for now{% endcomment %}

      <h2>GNU Hackers</h2>
      <ul>
{% for channel in Channels %}
  {% if channel.category == "GNU Hackers" %}
        <li>
    {% if not channel.message %}
          <a href="{{ channel.url }}" title="subscribe"><img src="static/feed-icon-10x10.png" alt="(feed)"></a>
          <a href="{% firstof channel.homepage channel.link %}" title="{{ channel.title|safe }}">{{ channel.name|safe }}</a>
    {% else %}
          <a {% if channel.url %}href="{{channel.url}}"{% endif %} title="subscribe"><img src="static/feed-icon-10x10.png" alt="(feed)"></a>
          <a class="message" {% if channel.homepage or channel.link %}href="{% firstof channel.homepage channel.link %}"{% endif %} title="{{ channel.message }}">{{ channel.name|safe }}</a>
    {% endif %}
        </li>
  {% endif %}
{% endfor %}
      </ul>
      </h2>

      <h2>Other subscriptions</h2>
      <ul>
{% for channel in Channels %}
  {% if channel.category == "Other subscriptions" %}
        <li>
    {% if not channel.message %}
          <a href="{{ channel.url }}" title="subscribe"><img src="static/feed-icon-10x10.png" alt="(feed)"></a>
          <a href="{% firstof channel.homepage channel.link %}" title="{{ channel.title|safe }}">{{ channel.name|safe }}</a>
    {% else %}
          <a {% if channel.url %}href="{{channel.url}}"{% endif %} title="subscribe"><img src="static/feed-icon-10x10.png" alt="(feed)"></a>
          <a class="message" {% if channel.homepage or channel.link %}href="{% firstof channel.homepage channel.link %}"{% endif %} title="{{ channel.message }}">{{ channel.name|safe }}</a>
    {% endif %}
        </li>
  {% endif %}
{% endfor %}
      </ul>
      </h2>

      <h2>Subscriptions from Savannah</h2>
      <ul>
{% for channel in Channels %}
  {% if channel.category == "Subscriptions from Savannah" %}
        <li>
    {% if not channel.message %}
          <a href="{{ channel.url }}" title="subscribe"><img src="static/feed-icon-10x10.png" alt="(feed)"></a>
          <a href="{% firstof channel.homepage channel.link %}" title="{{ channel.title|safe }}">{{ channel.name|safe }}</a>
    {% else %}
          <a {% if channel.url %}href="{{channel.url}}"{% endif %} title="subscribe"><img src="static/feed-icon-10x10.png" alt="(feed)"></a>
          <a class="message" {% if channel.homepage or channel.link %}href="{% firstof channel.homepage channel.link %}"{% endif %} title="{{ channel.message }}">{{ channel.name|safe }}</a>
    {% endif %}
        </li>
  {% endif %}
{% endfor %}
      </ul>
      </h2>

    </div>
  </body>
</html>
