planet-venus_0~git9de2109-4.2_all.deb from Debian 10 "buster"
  118518429fa94d19fc61135629a27b9fddc6327473fc2f45859aeee055fea27d
dependencies:
  apt-get install -f
  apt install python-django
  apt install bzip2 curl  # for 'savelog' and '17gnuprojects'
Patch dj.py from https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=893157
Patch dj.py again with:
https://docs.djangoproject.com/en/3.1/releases/1.11/#django-template-backends-django-template-render-prohibits-non-dict-context
--- dj.py	2020-08-15 05:39:24.570931228 +0000
+++ dj.py	2020-08-15 05:53:23.698043720 +0000
@@ -52,7 +52,7 @@
 
     # set up the Django context by using the default htmltmpl 
     # datatype converters
-    context = Context(autoescape=(config.django_autoescape()=='on'))
+    context = {}
     context.update(tmpl.template_info(doc))
     context['Config'] = config.planet_options()
     t = get_template(script)

Base templates + 'remove-trackers-and-ads.plugin' filter are from https://salsa.debian.org/planet-team/debian

mkdir git/
cd git/
#git clone https://salsa.debian.org/planet-team/config config
#git clone https://salsa.debian.org/planet-team/debian debian
git clone https://git.savannah.gnu.org/git/gnues/planet-config.git
git clone https://git.savannah.gnu.org/git/gnues/planet-infra.git
ln -s git/config
mkdir config/languages/
mkdir -p code/venus/
ln -s /usr/bin/planet code/venus/planet.py
ln -s ../git/debian/filters code/filters
mkdir log/
ln -s git/debian/www
ln -s ../../config/config www/config
ln -s ../../debian www/infra

ln -s $(pwd) /srv/planet.debian.org

Cron this:
run-parts git/debian/cron >/dev/null
